var isEnglish = false;

var ensureTranslation = function () {

    var imgLanguage = $('#img_language');
    imgLanguage.attr("src", "img/flag/flag-de_256.png");

    imgLanguage.click(function () {
        console.log('flag clicked');

        if (isEnglish) {
            imgLanguage.attr("src", "img/flag/flag-de_256.png");
            $('#nav_solutions')[0].innerHTML = "Start";
            $('#nav_services')[0].innerHTML = "Ihre Vorteile";
            $('#nav_technologies')[0].innerHTML = "Technologien";
            $('#nav_contact')[0].innerHTML = "Kontakt";
            $('#footer_copyright')[0].innerHTML = "© " + getYear() + " celox.io - Alle Rechte vorbehalten";
            $('#footer_privacy_and_terms')[0].innerHTML = "Impressum";

            // // header
            // $('#header_title')[0].innerHTML = "Steigern Sie Ihren Erfolg";
            // $('#header_subtitle')[0].innerHTML = "Unsere IT-Lösungen werden nach Ihren Bedürfnissen entwickelt und" +
            //     " erhöhen die Produktivität Ihres Unternehmens.";
            // $('#header_btn_forward')[0].innerHTML = "Mehr erfahren";
            //
            // // start
            // $('#start_title')[0].innerHTML = "Ihre Wünsche sind unser Auftrag";
            // $('#start_subtitle')[0].innerHTML = "Um Ihre Anwendung optimal auf Ihre Anforderungen abzustimmen," +
            //     " binden wir Sie in den Entwicklungsprozess ein. Sie bestimmen selbst über Umfang, Funktionalität" +
            //     " und Kosten. Unser Portfolio umfasst die Erstellung von Websites, Apps für mobile Geräte, sowie Desktop-Anwendungen.";
            // $('#solutions_btn_forward')[0].innerHTML = "Loslegen!";

            // navigation
            $('#nav_solutions')[0].innerHTML = "Start";
            $('#nav_services')[0].innerHTML = "Ihre Vorteile";
            $('#nav_technologies')[0].innerHTML = "Technologien";
            $('#nav_contact')[0].innerHTML = "Kontakt";

            // // services
            // $('#services_title')[0].innerHTML = "Ein gutes Konzept";
            // $('#services_item0_title')[0].innerHTML = "Planung";
            // $('#services_item0_subtitle')[0].innerHTML = "Sagen Sie uns welchen Zweck Ihre Anwendung verfolgt. Welche " +
            //     "Anforderungen haben Sie oder Ihre Kunden an das Produkt?";
            // $('#services_item1_title')[0].innerHTML = "Umsetzung";
            // $('#services_item1_subtitle')[0].innerHTML = "Profitieren Sie von frühen Vorab-Releases und gestalten auch" +
            //     " während der Entwicklung bei Funktionalität und Design mit.";
            // $('#services_item2_title')[0].innerHTML = "Auslieferung";
            // $('#services_item2_subtitle')[0].innerHTML = "Genießen Sie die Vorzüge professionell entwickelter" +
            //     " Software. Ausgiebige Tests gewährleisten, dass die Anwendung Ihren Wünschen entspricht.";
            // $('#services_item3_title')[0].innerHTML = "Support";
            // $('#services_item3_subtitle')[0].innerHTML = "Erhalten Sie regelmäßige Updates" +
            //     " und zuverlässigen Support, damit Ihre Anwendung stets auf dem neusten Stand ist.";
            // $('#service_btn_forward')[0].innerHTML = "Okay!";

            // technologies
            $('#technologies_title')[0].innerHTML = "Technologien";

            // contact
            $('#contact_title')[0].innerHTML = "Kontaktieren Sie uns";
            $('#contact_email_address')[0].innerHTML = "Email Adresse";
            $('#contact_email_address_in').attr("placeholder", "Email Adresse");
            $('#contact_phone_number')[0].innerHTML = "Telefonnummer";
            $('#contact_phone_number_in').attr("placeholder", "Telefonnummer");
            $('#contact_message')[0].innerHTML = "Nachricht";
            $('#contact_message_in').attr("placeholder", "Nachricht");
            $('#contact_btn_send')[0].innerHTML = "Senden";

            // footer
            $('#footer_copyright')[0].innerHTML = "© " + getYear() + " celox.io - Alle Rechte vorbehalten";
            $('#footer_privacy_and_terms')[0].innerHTML = "Impressum";


        } else {
            imgLanguage.attr("src", "img/flag/flag-en_256.png");
            $('#nav_solutions')[0].innerHTML = "Start";
            $('#nav_services')[0].innerHTML = "Advantages";
            $('#nav_technologies')[0].innerHTML = "Technologies";
            $('#nav_contact')[0].innerHTML = "Contact";
            $('#footer_copyright')[0].innerHTML = "© " + getYear() + " celox.io - All rights reserved";
            $('#footer_privacy_and_terms')[0].innerHTML = "Privacy and Terms";

        }
        isEnglish = !isEnglish;
    });

};


function init() {
    console.log('init');

    ensureTranslation();

    $('#modal_privacy_and_terms').on('show.bs.modal', function () {
        $('.modal .modal-body').css('overflow-y', 'auto');
        $('.modal .modal-body').css('max-height', $(window).height() * 0.7);
    });

}

