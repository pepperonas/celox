<!DOCTYPE html>
<html lang="de" xmlns="http://www.w3.org/1999/html">

<head>

  <link rel="stylesheet" type="text/css" href="css/styles.scss"/>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="App-Entwicklung in Darmstadt">
  <meta name="author" content="Sebastian Grätz, Martin Pfeffer">

  <title>App-Entwicklung in Darmstadt - celox.io</title>

  <!-- Bootstrap Core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
        rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
        rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS -->
  <link href="css/creative.css" rel="stylesheet">
  <link href="css/styles.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body id="page-top" onload="init()">

<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header pull-left">
      <a class="navbar-brand page-scroll" href="#page-top">Celox</a>
    </div>

    <div class="navbar-collapse navbar-right pull-right">
      <a id="lang_link" href="#" onclick="return false">
        <img id="img_language" class="navbar-brand navbar-flag" src="img/flag/flag-de_256.png" alt="language"
        >
      </a>
    </div>

    <div class="navbar-header pull-right">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a id="nav_solutions" class="page-scroll" href="#solutions">Leistungen</a>
        </li>
        <li>
          <a id="nav_services" class="page-scroll" href="#services">Ihre Vorteile</a>
        </li>
        <li>
          <a id="nav_technologies" class="page-scroll" href="#technologies">Technologien</a>
        </li>
        <li>
          <a id="nav_contact" class="page-scroll" href="#contact">Kontakt</a>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->

  </div>
  <!-- /.container -->
</nav>

<header>
  <div class="header-content">
    <div class="header-content-inner">
      <h1 id="header_title">Steigern Sie Ihren Erfolg</h1>
      <hr>
      <p id="header_subtitle">Wir bringen innovative Ideen mit sinnvollen Funktionen ins Hosentaschenformat.</p>
      <a id="header_btn_forward" href="#solutions" class="btn btn-primary btn-xl page-scroll">Mehr erfahren</a>
    </div>
  </div>
</header>

<section class="bg-primary" id="solutions">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 id="start_title" class="section-heading">Leistungen</h2>
        <hr class="light">
        <!--<p id="solutions_subtitle" class="text-faded">...</p>-->
        <div class="row">
          <div class="col-lg-12 text-center">
            <div class="col-lg-3 col-md-6 text-center">
              <div class="service-box">
                <i class="fa fa-4x fa-star-o text-primary sr-icons white-icon"></i>
                <h3 id="solutions_item0_title">Consulting</h3>
                <p id="solutions_item0_subtitle" class="text-muted white">Sie möchten Ihre Idee professionell
                  verwirklichen? Nehmen Sie Kontakt mit uns auf und wir planen gemeinsam den Weg von der Konzeption
                  bis hin zur Veröffentlichung.</p>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
              <div class="service-box">
                <i class="fa fa-4x fa-mobile text-primary sr-icons white-icon"></i>
                <h3 id="solutions_item1_title">Native App-Entwicklung</h3>
                <p id="solutions_item1_subtitle" class="text-muted white">Wir sind spezialisiert auf die native
                  Entwicklung von Anwendungen für Android und iOS, auch unter Einhaltung der jeweiligen Guidelines.</p>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
              <div class="service-box">
                <i class="fa fa-4x fa-globe text-primary sr-icons white-icon"></i>
                <h3 id="solutions_item2_title">Web-<br>Backend</h3>
                <p id="solutions_item2_subtitle" class="text-muted white">Benötigt Ihre App Zugriff auf einen
                  Server? Wir sorgen für den sicheren und reibungslos ablaufenden Datenaustausch.</p>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
              <div class="service-box">
                <i class="fa fa-4x fa-wrench text-primary sr-icons white-icon"></i>
                <h3 id="solutions_item3_title">Prozessoptimierung</h3>
                <p id="solutions_item4_subtitle" class="text-muted white">Durch den Einsatz von Apps lassen sich
                  Kosten für wiederkehrende Abläufe senken und die Qualität von Dienstleistungen erhöhen. Nutzen Sie
                  unsere Expertise.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="container text-center extra-padding-top">
          <a id="solutions_btn_forward" href="#services"
             class="page-scroll btn btn-default btn-xl sr-button">Gefällt mir!</a>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="services">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 id="services_title" class="section-heading">Ihre Vorteile</h2>
        <hr class="primary">
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6 text-center">
        <div class="service-box">
          <i class="fa fa-4x fa-magic text-primary sr-icons"></i>
          <h3 id="services_item0_title">Best practises</h3>
          <p id="services_item0_subtitle" class="text-muted text-black">Auch wenn bekanntlich viele Wege nach Rom
            führen, ist der Einsatz von strukturiertem und sorgfältig getestetem Code Teil unserer Kernkompetenz und
            wirkt sich maßgeblich auf das Endergebnis aus.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 text-center">
        <div class="service-box">
          <i class="fa fa-4x fa-handshake-o text-primary sr-icons"></i>
          <h3 id="services_item1_title">Klare Absprachen</h3>
          <p id="services_item1_subtitle" class="text-muted text-black">Getroffene Vereinbarungen und der
            regelmäßige Austausch mit dem Auftraggeber helfen uns Transparenz in den Entwicklungsprozess zu bringen.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 text-center">
        <div class="service-box">
          <i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i>
          <h3 id="services_item2_title">Publishing</h3>
          <p id="services_item2_subtitle" class="text-muted text-black">Sofern Sie es wünschen, kümmern wir uns um die
            Vermarktung Ihrer App, sowie um die Bereitstellung der Infrastruktur.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 text-center">
        <div class="service-box">
          <i class="fa fa-4x fa-life-ring text-primary sr-icons"></i>
          <h3 id="services_item3_title">Support</h3>
          <p id="services_item3_subtitle" class="text-muted text-black">Um langfristige Zufriedenheit zu
            gewährleisten, bieten wir regelmäßige Updates für Ihre Anwendung.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container text-center extra-padding-top">
    <a id="service_btn_forward" href="#contact" class="page-scroll btn btn-primary btn-xl sr-button">Klasse!</a>
  </div>
</section>

<section class="no-padding" id="technologies">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <h2 id="technologies_title" class="section-heading">Technologien</h2>
        <hr class="primary">
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row no-gutter popup-gallery">
      <!-- android -->
      <div class="col-lg-4 col-sm-6">
        <img src="img/technologies/thumbnails/android.png" class="img-responsive" alt="">
        <div class="technologies-box-caption">
          <div class="technologies-box-caption-content">
          </div>
        </div>
      </div>
      <!-- java -->
      <div class="col-lg-4 col-sm-6">
        <img src="img/technologies/thumbnails/java.png" class="img-responsive" alt="">
        <div class="technologies-box-caption">
          <div class="technologies-box-caption-content">
          </div>
        </div>
      </div>
      <!-- mysql -->
      <div class="col-lg-4 col-sm-6">
        <img src="img/technologies/thumbnails/my_sql.png" class="img-responsive" alt="">
        <div class="technologies-box-caption">
          <div class="technologies-box-caption-content">
          </div>
        </div>
      </div>
      <!-- html -->
      <div class="col-lg-4 col-sm-6">
        <img src="img/technologies/thumbnails/html.png" class="img-responsive" alt="">
        <div class="technologies-box-caption">
          <div class="technologies-box-caption-content">
          </div>
        </div>
      </div>
      <!-- angular -->
      <div class="col-lg-4 col-sm-6">
        <img src="img/technologies/thumbnails/angular.png" class="img-responsive" alt="">
        <div class="technologies-box-caption">
          <div class="technologies-box-caption-content">
          </div>
        </div>
      </div>
      <!-- php -->
      <div class="col-lg-4 col-sm-6">
        <img src="img/technologies/thumbnails/php.png" class="img-responsive" alt="">
        <div class="technologies-box-caption">
          <div class="technologies-box-caption-content">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 id="contact_title">Kontaktieren Sie uns</h2>
        <hr class="star-primary">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <form id="contact-form" method="post" action="contact.php" role="form">

          <div class="messages"></div>

          <div class="controls">

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="form_name">Firstname *</label>
                  <input id="form_name" type="text" name="name" class="form-control"
                         placeholder="Please enter your firstname *" required="required"
                         data-error="Firstname is required.">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="form_lastname">Lastname *</label>
                  <input id="form_lastname" type="text" name="surname" class="form-control"
                         placeholder="Please enter your lastname *" required="required"
                         data-error="Lastname is required.">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="form_email">Email *</label>
                  <input id="form_email" type="email" name="email" class="form-control"
                         placeholder="Please enter your email *" required="required"
                         data-error="Valid email is required.">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="form_phone">Phone</label>
                  <input id="form_phone" type="tel" name="phone" class="form-control"
                         placeholder="Please enter your phone">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="form_message">Message *</label>
                  <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *"
                            rows="4" required="required" data-error="Please,leave us a message."></textarea>
                  <div class="help-block with-errors"></div>
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <!-- generated at https://www.google.com/recaptcha/admin -->
                  <div class="g-recaptcha" data-sitekey="6LdbuhUUAAAAAKKEoP2zX2J39y1pmCzFXOvhAsBP"></div>
                </div>
              </div>

              <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Send message">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> required.</p>
              </div>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</section>

<footer class="text-center">
  <div class="footer-below">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div id="footer_copyright">
            &copy; 2017 celox.io - Alle Rechte vorbehalten
          </div>
          <a href="#" id="footer_privacy_and_terms" class="footer-privacy-and-terms" data-toggle="modal"
             data-target="#modal_privacy_and_terms">Impressum</a>
          <!-- modal -->
          <div class="modal fade" id="modal_privacy_and_terms" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <div class="modal-body" id="dialog-privacy-terms">
                    <h3 class="modal_title">Impressum</h3>
                    <div class="modal_head">
                      <div class="modal_head_title">celox.io</div>
                      <p class="modal_head_content">Pfeffer / Grätz - GbR<br>
                        E-Mail: <a class="adr_mail" href="mailto:martinpaush@gmail.com">
                          &ensp;&ensp;martinpaush@gmail.com</a><br>
                        Telefon: <a class="adr_mail" href="tel:+4915159082465">
                          &ensp;+49 151 590 824 65</a></p>
                      <p class="modal_head_content">Vertreten durch:<br>
                        Martin Pfeffer & Sebastian Grätz<br>
                        Weidigweg 17<br>
                        64297 Darmstadt</p>
                      <p class="modal_head_content">Steuernummer:
                        -&#45;&#45;/&#45;&#45;&#45;&#45;/&#45;&#45;&#45;&#45;</p>
                    </div>
                    <br>
                    <div class="modal_content_title">Datenschutz</div>
                    <p class="modal_content_content">Nachfolgend möchten wir Sie über unsere Datenschutzerklärung
                      informieren. Sie finden
                      hier Informationen über die Erhebung und Verwendung persönlicher Daten bei der
                      Nutzung unserer Webseite. Wir beachten dabei das für Deutschland geltende
                      Datenschutzrecht. Sie können diese Erklärung jederzeit auf unserer Webseite abrufen.
                      <br><br>
                      Wir weisen ausdrücklich darauf hin, dass die Datenübertragung im Internet (z.B. bei
                      der Kommunikation per E-Mail) Sicherheitslücken aufweisen und nicht lückenlos vor
                      dem Zugriff durch Dritte geschützt werden kann.
                      <br><br>
                      Die Verwendung der Kontaktdaten unseres Impressums zur gewerblichen Werbung ist
                      ausdrücklich nicht erwünscht, es sei denn wir hatten zuvor unsere schriftliche
                      Einwilligung erteilt oder es besteht bereits eine Geschäftsbeziehung. Der Anbieter
                      und alle auf dieser Website genannten Personen widersprechen hiermit jeder
                      kommerziellen Verwendung und Weitergabe ihrer Daten.
                    </p>
                    <div class="modal_content_subtitle">Personenbezogene Daten</div>
                    <p class="modal_content_content">Sie können unsere Webseite ohne Angabe personenbezogener Daten
                      besuchen. Soweit auf
                      unseren Seiten personenbezogene Daten (wie Name, Anschrift oder E-Mail Adresse)
                      erhoben werden, erfolgt dies, soweit möglich, auf freiwilliger Basis. Diese Daten
                      werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben. Sofern
                      zwischen Ihnen und uns ein Vertragsverhältnis begründet, inhaltlich ausgestaltet
                      oder geändert werden soll oder Sie an uns eine Anfrage stellen, erheben und
                      verwenden wir personenbezogene Daten von Ihnen, soweit dies zu diesen Zwecken
                      erforderlich ist (Bestandsdaten). Wir erheben, verarbeiten und nutzen
                      personenbezogene Daten soweit dies erforderlich ist, um Ihnen die Inanspruchnahme
                      des Webangebots zu ermöglichen (Nutzungsdaten). Sämtliche personenbezogenen Daten
                      werden nur solange gespeichert wie dies für den geannten Zweck (Bearbeitung Ihrer
                      Anfrage oder Abwicklung eines Vertrags) erforderlich ist. Hierbei werden steuer- und
                      handelsrechtliche Aufbewahrungsfristen berücksichtigt. Auf Anordnung der zuständigen
                      Stellen dürfen wir im Einzelfall Auskunft über diese Daten (Bestandsdaten) erteilen,
                      soweit dies für Zwecke der Strafverfolgung, zur Gefahrenabwehr, zur Erfüllung der
                      gesetzlichen Aufgaben der Verfassungsschutzbehörden oder des Militärischen
                      Abschirmdienstes oder zur Durchsetzung der Rechte am geistigen Eigentum erforderlich
                      ist.</p>
                    <div class="modal_content_subtitle">Datenschutzerklärung für Google Analytics
                    </div>
                    <p class="modal_content_content">Diese Website benutzt Google Analytics, einen Webanalysedienst
                      der Google Inc.
                      ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem
                      Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie
                      ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser
                      Website werden in der Regel an einen Server von Google in den USA übertragen und
                      dort gespeichert. Wir haben die IP-Anonymisierung aktiviert. Auf dieser Webseite
                      wird Ihre IP-Adresse von Google daher innerhalb von Mitgliedstaaten der Europäischen
                      Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen
                      Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an
                      einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des
                      Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung
                      der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen
                      und um weitere mit der Websitenutzung und der Internetnutzung verbundene
                      Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von
                      Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen
                      Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine
                      entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch
                      darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser
                      Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung
                      der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten
                      (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google
                      verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin
                      herunterladen und installieren:
                      <a class="adr_mail"
                         href="http://tools.google.com/dlpage/gaoptout?hl=de"
                         rel="nofollow" target="_blank">http://tools.google.com/dlpage/gaoptout?hl=de</a>
                    </p>
                    <p class="modal_content_content">
                      Das Speichern von Cookies auf Ihrer Festplatte können Sie verhindern, indem Sie in
                      Ihren Browser-Einstellungen "keine Cookies akzeptieren" wählen (Im MS
                      Internet-Explorer unter "Extras > Internetoptionen > Datenschutz > Einstellung"; im
                      Firefox unter "Extras > Einstellungen > Datenschutz > Cookies"); wir weisen Sie
                      jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen
                      dieser Website voll umfänglich nutzen können. Durch die Nutzung dieser Website
                      erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in
                      der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck
                      einverstanden. Weitere Informationen darüber wie Google Conversion-Daten verwendet
                      sowie die Datenschutzerklärung von Google finden Sie unter:
                      <a class="adr_mail"
                         href="https://support.google.com/adwords/answer/93148?ctx=tltp"
                         target="_blank">https://support.google.com/adwords/answer/93148?ctx=tltp</a>,
                      <a class="adr_mail" href="http://www.google.de/policies/privacy/" target="_blank">http://www.google.de/policies/privacy/</a>
                    </p><br>
                    <div class="modal_content_title">Haftungsausschluss</div>
                    <p class="modal_content_content">Alle Angaben und Daten wurden nach bestem Wissen erstellt, es
                      wird jedoch keine
                      Gewähr für deren Vollständigkeit und Richtigkeit übernommen.<br>
                      Wir behalten uns das Recht vor,
                      ohne vorherige Ankündigung die
                      bereitgestellten
                      Informationen zu ändern, zu ergänzen oder zu entfernen.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- modal end -->

        </div>
      </div>
    </div>
  </div>
</footer>

<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="js/validator.js"></script>
<script src="js/contact.js"></script>

<!-- Theme JavaScript -->
<script src="js/creative.js"></script>
<script src="js/app.js"></script>
<script src="js/lib.js"></script>

</body>

</html>
